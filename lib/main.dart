import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

List<CameraDescription> cameras;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  cameras = await availableCameras();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  File file;
  CameraController cameraController;
  void realTimeImage() async {
    cameraController = CameraController(cameras.first, ResolutionPreset.max);
    cameraController.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  void takePick() {
    cameraController.takePicture().then((value) {
      file = File(value.path);
      setState(() {});
    });
  }

  @override
  void initState() {
    super.initState();
    realTimeImage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextButton(
            onPressed: () {
              takePick();
            },
            child: Text('Take a photo'),
          ),
          SizedBox(
              width: double.infinity,
              height: 300,
              child: CameraPreview(cameraController)),
          file != null
              ? SizedBox(
                  width: double.infinity,
                  height: 250,
                  child:
                      FittedBox(fit: BoxFit.contain, child: Image.file(file)))
              : SizedBox(),
        ],
      ),
    );
  }
}
